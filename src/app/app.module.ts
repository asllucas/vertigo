import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StarwarsComponent } from './starwars/components/starwars/starwars.component';
import { StarwarsDropdownComponent } from './starwars/components/starwars-dropdown/starwars-dropdown.component';

@NgModule({
  declarations: [
    AppComponent,
    StarwarsComponent,
    StarwarsDropdownComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
