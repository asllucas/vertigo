import { Component, OnInit } from '@angular/core';
import { StarWarsApi } from './starwars/api/starwars.api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Star Wars';
  public people = [];
  public cachePeoples = [];
  public removePeople;

  constructor(private api: StarWarsApi) { }

  ngOnInit(): void {

    this.getPeoples();
  }

  private getPeoples() {
    this.api.getAllPeoples().subscribe(res => {
      this.people = res;
      this.cachePeoples = this.people;
    });
  }

  public remove(people) {
    const index = this.people.indexOf(people);
    this.removePeople = this.people.splice(index, 1);
  }
}
