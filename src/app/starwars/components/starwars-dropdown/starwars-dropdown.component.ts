import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StarWarsApi } from '../../api/starwars.api';
import { StarWars } from '../../models/starwars';

@Component({
  selector: 'app-starwars-dropdown',
  templateUrl: './starwars-dropdown.component.html',
  styleUrls: ['./starwars-dropdown.component.css']
})
export class StarwarsDropdownComponent implements OnInit {

  @Input() peoples: StarWars[];
  @Input() cachePeoples: StarWars[];

  constructor() { }

  ngOnInit(): void {
  }

  filterPeoples(filterVal: any) {
    if (filterVal == "0")
      this.peoples = this.cachePeoples;
    else
      this.peoples = this.cachePeoples.filter((item) => item.name == filterVal);
  }

}
