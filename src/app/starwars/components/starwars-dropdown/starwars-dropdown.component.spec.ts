import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StarwarsDropdownComponent } from './starwars-dropdown.component';

describe('StarwarsDropdownComponent', () => {
  let component: StarwarsDropdownComponent;
  let fixture: ComponentFixture<StarwarsDropdownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StarwarsDropdownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StarwarsDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
