import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StarWars } from '../../models/starwars';

@Component({
  selector: 'app-starwars',
  templateUrl: './starwars.component.html',
  styleUrls: ['./starwars.component.css']
})
export class StarwarsComponent implements OnInit {

  @Input() peoples : StarWars[];
  @Output() onRemove = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    
  }

}
