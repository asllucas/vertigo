import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { StarWarsApi } from "./starwars.api"
import { HttpClientTestingModule,
    HttpTestingController } from '@angular/common/http/testing';

describe("StarWarsApi", () => {
    let starWarsApi: StarWarsApi;
    let httpTestingController : HttpTestingController

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports:[
                HttpClientTestingModule
            ],
            providers: [
                StarWarsApi,
                HttpClient
            ]
        });
        httpTestingController = TestBed.get(HttpTestingController);
        starWarsApi = TestBed.get(StarWarsApi)
    })

    afterEach(() => {
        httpTestingController.verify();
      });

    it("should retrieve all peoples", () => {
        expect(starWarsApi).toBeTruthy();
    })
})