import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { StarWars } from '../models/starwars';

@Injectable({ providedIn: 'root' })
export class StarWarsApi {

    private url = "https://swapi.dev/api/people/";
    
    constructor(private http : HttpClient){}

    public getAllPeoples() : Observable<StarWars[]>{
        return this.http.get<StarWars[]>(this.url).pipe(
            map(data => {
                return data['results'];
            })
        )
    }

}